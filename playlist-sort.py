#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Program to sort static Rhythmbox playlists
"""


from __future__ import annotations

import argparse
import enum
from functools import total_ordering
import os
from pathlib import Path
import re
from typing import Any, Callable, Optional
import urllib
import sys

from lxml import etree
import mutagen


ALPHA_NUMERIC_RE: re.Pattern = re.compile(r'[^a-zA-Z\d ]', re.A)
"""
Regex to distill strings down to alpha-numeric characters (and space).

By matching ASCII only some symbols will slip through, but that's acceptable.
Important that non-ASCII words (kanji, etc.) get through.
"""

RC_NORMAL: int = 0
"""Return code for normal execution."""

RC_ARGUMENT_ERROR: int = 1
"""Return code in the case of an illegal CLI argument."""

RC_PLAYLIST_NOT_FOUND: int = 2
"""Return code if a specified playlist name is not found in the file parsed."""


class SortType(enum.Enum):
    """Enumeration for all the supported sorting methods."""

    # Enum values are constants
    # pylint: disable=C0103

    ARTIST = enum.auto()
    ALBUM  = enum.auto()
    STANDARD = enum.auto()
    TITLE = enum.auto()

@total_ordering
class Song(): # pylint: disable=R0902
    """
    Generalized Song object for holding metadata.

    Audio file formats come in a variety of forms with distinct tagging
    schemes. This object parses such files into a unified format for the purpose
    of sorting.

    Attributes should be treated an immutable.

    Attributes:
        artist:
            The song's artist from the file tags, or None.
        artistLower:
            Lowercase of the artist, or None.
        artistLowerAlphanum:
            Lowercase alphanumeric of the artist, or None.
        albumArtist:
            The song's album artist from the file tags, or None.
        albumArtistLower:
            Lowercase of the album artist, or None.
        albumArtistLowerAlphanum:
            Lowercase alphanumeric of the album artist, or None.
        album:
            The song's album from the file tags, or None.
        albumLower:
            Lowercase of the album, or None.
        albumLowerAlphanum:
            Lowercase alphanumeric of the album, or None.
        disk:
            Disk number of the song, or None.
        location:
            URI file location from the Rhythmbox XML.
        title:
            Song's title from the file tags, or the filename if there is no
            title tag in the audio file.
        titleLower:
            Lowercase of the title.
        titleLowerAlphanum:
            Lowercase alphanumeric of the title.
        track:
            Track number of the song from the file tags, or None.
        year:
            Album year from the file tags, or None.
    """

    def __init__(
        self, location: str, sortType: SortType = SortType.STANDARD
    ):
        """
        Initialize Song object.

        Args:
            location:
                URI of the song from the Rhythmbox XML. Must be on local
                filesystem and not remote.
            sortType:
                Sorting method to use. Optional. Default is STANDARD.
        Raises:
            AssertionError:
                If the sorting method has been incorrectly defined.
            IOError:
                If the file backing the song cannot be found, or Mutagen
                failed to parse the file.
            NotImplementedError:
                If the sorting method specified has not been not implemented.
            RuntimeError:
                If the song file type cannot be parsed for tags.
                Functionality for parsing has only been implemented for
                AIFF, MP3, MP4, and Opus.
        """

        self.artist: Optional[str] = None
        self.artistLower: Optional[str] = None
        self.artistLowerAlphanum: Optional[str] = None
        self.albumArtist: Optional[str] = None
        self.albumArtistLower: Optional[str] = None
        self.albumArtistLowerAlphanum: Optional[str] = None
        self.album: Optional[str] = None
        self.albumLower: Optional[str] = None
        self.albumLowerAlphanum: Optional[str] = None
        self.disk: Optional[int] = None
        self.title: str
        self.titleLower: str
        self.titleLowerAlphanum: str
        self.track: Optional[int] = None
        self.year: Optional[int] = None

        self.location: str = location

        # mypy can't find urllib.parse for some reason
        self._path: Path = Path(urllib.parse.unquote(   # type: ignore
            urllib.parse.urlparse(location).path        # type: ignore
        ))

        # Choose the comparison function
        self._eqFuncs: list[Callable[[Song, Song], bool]]
        self._ltFuncs: list[Callable[[Song, Song], bool]]
        if sortType == SortType.ALBUM:
            self._eqFuncs = ALBUM_EQ_FUNCS
            self._ltFuncs = ALBUM_LT_FUNCS
        elif sortType == SortType.ARTIST:
            self._eqFuncs = ARTIST_EQ_FUNCS
            self._ltFuncs = ARTIST_LT_FUNCS
        elif sortType == SortType.STANDARD:
            self._eqFuncs = STANDARD_EQ_FUNCS
            self._ltFuncs = STANDARD_LT_FUNCS
        elif sortType == SortType.TITLE:
            self._eqFuncs = TITLE_EQ_FUNCS
            self._ltFuncs = TITLE_LT_FUNCS
        else:
            raise NotImplementedError(
                "Comparison methods not implemented for sort type {sortType}"
            )
        assert len(self._eqFuncs) == len(self._ltFuncs), (
            "There must be a matching number of lt functions for every eq "
            "function specified for use"
        )

        # Let Mutagen errors propagate
        tags = mutagen.File(self._path)

        if isinstance(tags, (mutagen.aiff.AIFF, mutagen.mp3.MP3)):
            self._parse_mp3(tags)
        elif isinstance(tags, mutagen.mp4.MP4):
            self._parse_mp4(tags)
        elif isinstance(tags, mutagen.oggopus.OggOpus):
            self._parse_opus(tags)
        else:
            # Too lazy to use the logger
            print(tags)
            print(self._path)
            raise RuntimeError(
                f"Unknown tag type {type(tags)}"
            )

        self._set_alternative_tags()

    def __eq__(self, other: Any) -> bool:

        if not isinstance(other, Song):
            return False

        for func in self._eqFuncs:
            if not func(self, other):
                return False

        return True

    def __lt__(self, other: Any) -> bool:

        if not isinstance(other, Song):
            raise TypeError("Cannot compare a Song to a non-Song object")

        i: int = 0
        for i, func in enumerate(self._eqFuncs):
            if not func(self, other):
                break
        # If the two Songs are equal, arbitrarily return True
        else:
            return True

        return self._ltFuncs[i](self, other)


    def _parse_mp3(self, tags: mutagen.mp3.MP3) -> None:
        """
        Parse Mutagen AIFF/MP3 tags.

        Warning: AIFF tag testing was poor due to a limited sample set.
        AIFF parsing may fail.
        """

        if 'TPE1' in tags.keys() and tags['TPE1'].text:
            self.artist = tags['TPE1'].text[0]

        if 'TPE2' in tags.keys() and tags['TPE2'].text:
            self.albumArtist = tags['TPE2'].text[0]

        if 'TALB' in tags.keys() and tags['TALB'].text:
            self.album = tags['TALB'].text[0]

        if 'TRCK' in tags.keys() and tags['TRCK'].text:
            track: list[str] = tags['TRCK'].text[0].split('/')
            if track and track[0].isdigit():
                self.track = int(track[0])

        if 'TPOS' in tags.keys() and tags['TPOS'].text:
            disk: list[str] = tags['TPOS'].text[0].split('/')
            if disk and disk[0].isdigit():
                self.disk = int(disk[0])

        # In Rhythmbox, if a song does not have a title the filename is
        # displayed instead. We shall replicate that here.
        # Every Song object must have some title
        if 'TIT2' in tags.keys() and tags['TIT2'].text:
            self.title = tags['TIT2'].text[0]
        else:
            self.title = self._path.name

        if (
            'TDRC' in tags.keys()
            and tags['TDRC'].text
            and str(tags['TDRC'].text[0]).isdigit()
        ):
            self.year = int(str(tags['TDRC'].text[0]))

    def _parse_mp4(self, tags: mutagen.mp4.MP4) -> None:

        if '\xa9ART' in tags.keys() and tags['\xa9ART']:
            self.artist = tags['\xa9ART'][0]

        if 'aART' in tags.keys() and tags['aART']:
            self.albumArtist = tags['aART'][0]

        if '\xa9alb' in tags.keys() and tags['\xa9alb']:
            self.album = tags['\xa9alb'][0]

        if 'disk' in tags.keys() and tags['disk'] and tags['disk'][0]:
            self.disk = tags['disk'][0][0]

        # In Rhythmbox, if a song does not have a title the filename is
        # displayed instead. We shall replicate that here.
        # Every Song object must have some title
        if '\xa9nam' in tags.keys() and tags['\xa9nam']:
            self.title = tags['\xa9nam'][0]
        else:
            self.title = self._path.name

        if 'trkn' in tags.keys() and tags['trkn'] and tags['trkn'][0]:
            self.track = tags['trkn'][0][0]

        if (
            '\xa9day' in tags.keys()
            and tags['\xa9day']
            and tags['\xa9day'][0].isdigit()
        ):
            self.year = int(tags['\xa9day'][0])

    def _parse_opus(self, tags: mutagen.oggopus.OggOpus) -> None:

        if 'artist' in tags.keys() and tags['artist']:
            self.artist = tags['artist'][0]

        if 'albumartist' in tags.keys() and tags['albumartist']:
            self.albumArtist = tags['albumartist'][0]

        if 'album' in tags.keys() and tags['album']:
            self.album = tags['album'][0]

        if (
            'discnumber' in tags.keys()
            and tags['discnumber']
            and tags['discnumber'][0].isdigit()
        ):
            self.disk = int(tags['discnumber'][0])

        # In Rhythmbox, if a song does not have a title the filename is
        # displayed instead. We shall replicate that here.
        # Every Song object must have some title
        if 'title' in tags.keys() and tags['title']:
            self.title = tags['title'][0]
        else:
            self.title = self._path.name

        if (
            'tracknumber' in tags.keys()
            and tags['tracknumber']
            and tags['tracknumber'][0].isdigit()
        ):
            self.track = int(tags['tracknumber'][0])

        if (
            'date' in tags.keys()
            and tags['date']
            and tags['date'][0].isdigit()
        ):
            self.year = int(tags['date'][0])

    def _set_alternative_tags(self) -> None:
        """Set all the alternative tag attributes."""

        if self.album is not None:
            self.albumLower = self.album.lower()
            self.albumLowerAlphanum = ALPHA_NUMERIC_RE.sub('', self.albumLower)
        if self.artist is not None:
            self.artistLower = self.artist.lower()
            self.artistLowerAlphanum = ALPHA_NUMERIC_RE.sub(
                '', self.artistLower
            )
        if self.albumArtist is not None:
            self.albumArtistLower = self.albumArtist.lower()
            self.albumArtistLowerAlphanum = ALPHA_NUMERIC_RE.sub(
                '', self.albumArtistLower
            )
        self.titleLower = self.title.lower()
        self.titleLowerAlphanum = ALPHA_NUMERIC_RE.sub('', self.titleLower)


def _eq_artist(a: Song, b: Song) -> bool:
    """Equality for artist name."""
    if a.artistLower is None or b.artistLower is None:
        return a.albumArtistLower == b.albumArtistLower
    return a.artistLower == b.artistLower

def _lt_artist(a: Song, b: Song) -> bool:
    """Less-than comparison for artist name."""
    if a.artistLower is None or b.artistLower is None:
        _lt_comp(a.albumArtistLower, b.albumArtistLower)
    return _lt_comp(a.artistLower, b.artistLower)

def _eq_artist_alphanum(a: Song, b: Song) -> bool:
    """Equality for alphanumeric artist name."""
    if a.artistLowerAlphanum is None or b.artistLowerAlphanum is None:
        return a.albumArtistLowerAlphanum == b.albumArtistLowerAlphanum
    return a.artistLowerAlphanum == b.artistLowerAlphanum

def _lt_artist_alphanum(a: Song, b: Song) -> bool:
    """Less-than comparison for alphanumeric artist name."""
    if a.artistLowerAlphanum is None or b.artistLowerAlphanum is None:
        _lt_comp(
            a.albumArtistLowerAlphanum,
            b.albumArtistLowerAlphanum
        )
    return _lt_comp(a.artistLowerAlphanum, b.artistLowerAlphanum)

def _eq_album(a: Song, b: Song) -> bool:
    """Equality for album name."""
    return a.albumLower == b.albumLower

def _lt_album(a: Song, b: Song) -> bool:
    """Less-than comparison for album name."""
    return _lt_comp(a.albumLower, b.albumLower)

def _eq_album_alphanum(a: Song, b: Song) -> bool:
    """Equality for alphanumeric album name."""
    return a.albumLowerAlphanum == b.albumLowerAlphanum

def _lt_album_alphanum(a: Song, b: Song) -> bool:
    """Less-than comparison for alphanumeric album name."""
    return _lt_comp(a.albumLowerAlphanum, b.albumLowerAlphanum)

def _eq_disk(a: Song, b: Song) -> bool:
    """Equality for disk number."""
    return a.disk == b.disk

def _lt_disk(a: Song, b: Song) -> bool:
    """Less-than comparison for disk number."""
    return _lt_comp(a.disk, b.disk)

def _eq_title (a: Song, b: Song) -> bool:
    """Equality for song title."""
    return a.titleLower == b.titleLower

def _lt_title(a: Song, b: Song) -> bool:
    """Less-than comparison for song title."""
    return a.titleLower < b.titleLower

def _eq_title_alphanum(a: Song, b: Song) -> bool:
    """Equality for alphanumeric song title."""
    return a.titleLowerAlphanum == b.titleLowerAlphanum

def _lt_title_alphanum(a: Song, b: Song) -> bool:
    """Less-than comparison for alphanumeric song title."""
    return a.titleLowerAlphanum < b.titleLowerAlphanum

def _eq_track(a: Song, b: Song) -> bool:
    """Equality for disk track number"""
    return a.track == b.track

def _lt_track(a: Song, b: Song) -> bool:
    """Less-than comparison for disk track number."""
    return _lt_comp(a.track , b.track)

def _eq_year(a: Song, b: Song) -> bool:
    """Equality for album year."""
    return a.year == b.year

def _lt_year(a: Song, b: Song) -> bool:
    """Less-than comparison for album year."""
    return _lt_comp(a.year, b.year)

def _lt_comp(a: Any, b: Any) -> bool:
    """Less-than comparison helper common function."""
    return b is None or (a is not None and a < b)


# To make it easier to add new sorting methods, __eq__ and __lt__ are
# generalized methods that take in lists of callables.
# The functions are executed in order to determine the comparison outcome.
#
# Ease of adding functionality is more important than program performance.
# The performance hit to this sorting method is not observable given how
# IO-bound and Mutagen-bound this utility is.

# Ordered equality functions for songs in album order
ALBUM_EQ_FUNCS: list[Callable[[Song, Song], bool]] = [
    _eq_album_alphanum,
    _eq_artist_alphanum,
    _eq_disk,
    _eq_track,
    _eq_title_alphanum
]
# Ordered functions for comparing songs in album order
ALBUM_LT_FUNCS: list[Callable[[Song, Song], bool]] = [
    _lt_album_alphanum,
    _lt_artist_alphanum,
    _lt_disk,
    _lt_track,
    _lt_title_alphanum
]

# Ordered equality functions for songs in artist order
ARTIST_EQ_FUNCS: list[Callable[[Song, Song], bool]] = [
    _eq_artist_alphanum,
    _eq_album_alphanum,
    _eq_disk,
    _eq_track,
    _eq_title_alphanum
]
# Ordered functions for comparing songs in artist order
ARTIST_LT_FUNCS: list[Callable[[Song, Song], bool]] = [
    _lt_artist_alphanum,
    _lt_album_alphanum,
    _lt_disk,
    _lt_track,
    _lt_title_alphanum
]

# Ordered equality functions for songs in standard order
STANDARD_EQ_FUNCS: list[Callable[[Song, Song], bool]] = [
    _eq_artist,
    _eq_year,
    _eq_album,
    _eq_disk,
    _eq_track,
    _eq_title
]
# Ordered functions for comparing songs in standard order
STANDARD_LT_FUNCS: list[Callable[[Song, Song], bool]] = [
    _lt_artist,
    _lt_year,
    _lt_album,
    _lt_disk,
    _lt_track,
    _lt_title
]

# Ordered equality functions for songs in title order
TITLE_EQ_FUNCS: list[Callable[[Song, Song], bool]] = [
    _eq_title_alphanum,
    _eq_artist_alphanum,
    _eq_album_alphanum,
    _eq_disk,
    _eq_track
]
# Ordered functions for comparing songs in title order
TITLE_LT_FUNCS: list[Callable[[Song, Song], bool]] = [
    _lt_title_alphanum,
    _lt_artist_alphanum,
    _lt_album_alphanum,
    _lt_disk,
    _lt_track
]


def _parse_args() -> argparse.Namespace:
    """
    Parse CLI arguments.

    Returns:
        argparse Namespace of CLI arguments.
    """

    parser = argparse.ArgumentParser(
        description='Sort static Rhythmbox playlists',
        epilog=(
            'It is suggested not to run this program while Rhythmbox is open. '
            'Behavior in that situation is undefined. '
            'It is also suggested that a backup is made of the playlists.xml '
            'file, if the resulting sorting order is not as desired. '
            'Once this program overwrites the playlists file, the original '
            'playlist ordering is not recoverable.'
        )
    )

    parser.add_argument(
        '--all',
        action='store_true',
        default=False,
        help='Sort all static playlists.'
    )
    parser.add_argument(
        '--dry-run',
        action='store_true',
        default=False,
        help='Print sorted XML instead of overwriting playlists file.'
    )
    parser.add_argument(
        '--location',
        default=os.path.expanduser('~/.local/share/rhythmbox/playlists.xml'),
        help=(
            'Location of playlists XML to sort, instead of using the default '
            'Rhythmbox file at ~/.local/share/rhythmbox/playlists.xml.'
        )
    )
    parser.add_argument(
        '--playlist',
        help=(
            'Name of the playlist to sort. '
            'Required argument if --all is not used.'
        )
    )
    parser.add_argument(
        "--sort-type",
        choices=[val.name.lower() for val in SortType],
        default=SortType.STANDARD.name,
        help='Sorting method to use.'
    )

    return parser.parse_args()


def get_songs(
    playlist: etree._element,
    sortType: SortType = SortType.STANDARD
) -> list[Song]:
    """
    Get a list of song objects from an XML playlist element.

    Args:
        playlist:
            lxml element playlist.
        sortType:
            Sorting method to use. Optional. Default is STANDARD.

    Returns:
        List of Song objects from the playlist.

    Raises:
        AssertionError:
            If the sorting method has been incorrectly defined.
        IOError:
            If a file backing a song cannot be found, or Mutagen
            fails to parse the file.
        NotImplementedError:
            If the sorting method specified has not been not implemented.
        RuntimeError:
            If a song file type cannot be parsed for tags.
            Functionality for parsing has only been implemented for
            AIFF, MP3, MP4, and Opus.
    """

    songs: list[Song] = []

    # Thread overhead costs more than the IO from these small file reads given
    # all the logic Mutagen has to do when reading the file.
    # Attempts to read the files into memory parallelized, and then load the
    # byte buffers into Mutagen, proved very inefficient.
    #
    # Parallelization is harmful. Basically doubles the runtime given ~2k
    # songs.

    element: etree._Element
    for element in playlist:
        location = element.text.strip()
        if location:
            songs.append(Song(location, sortType=sortType))

    return songs

def xml_get_playlists(
    tree: etree._ElementTree, playlistName: Optional[str] = None
) -> list[etree._Element]:
    """
    Get static playlist elements out of lxml tree.

    Args:
        tree:
            lxml tree.
        playistName:
            The name of a playlist to select. Optional.
            If None, all static playlists found will be returned.

    Returns:
        List of lxml playlist elements found.
    """

    playlists: list[etree._Element]
    if playlistName is not None:
        playlists = tree.xpath(
            f'//playlist[@type="static" and @name="{playlistName}"]'
        )
    else:
        playlists = tree.xpath('//playlist[@type="static"]')

    return playlists

def xml_update(playlist: etree._Element, songs: list[Song]) -> None:
    """
    Update an lxml playlist element to match a list of Songs.

    Args:
        playlist: The lxml playlist element to update.
        songs:    The ordered list of songs to update the lxml element with.
    """

    # Clear the playlist
    # Re-build instead of trying to sort in-place using XML methods
    playlist[:] = []

    elements: etree._Element = []
    song: Song
    for song in songs:

        element = etree.Element("location")
        element.text = song.location
        elements.append(element)

    playlist.extend(elements)

def xml_write(
    filepath: str, tree: etree._ElementTree, dryRun: bool = False
) -> None:
    """
    Update the Rhythmbox XML file with an updated list.

    Args:
        filepath:
            Path to XML file.
        tree:
            XML tree to write to the file.
        dryRun:
            Whether or not to write to stdout instead of to the XML file.
            Optional. Default is False.

    Raises:
        IOError: if the file cannot be written.
    """

    if dryRun:
        print(
            etree.tostring(tree, xml_declaration=True, pretty_print=True)
                .decode("utf-8")
                .strip()
        )
    else:
        tree.write(
            filepath, xml_declaration=True, pretty_print=True
        )


def main() -> int:
    """
    Program entry-point.

    Returns:
        0: If execution completed normally.
        1: If a CLI argument is invalid.
        2: If the playlist specified by --playlist CLI argument could not be
           found.
    """

    args: argparse.Namespace = _parse_args()

    if not args.all and not args.playlist:
        print(
            "Must specify a playlist to sort using --playlist, "
            "or must specify that all that playlists are to be sorted using "
            "--all."
        )
        return RC_ARGUMENT_ERROR

    if not os.path.exists(args.location):
        print(f"XML does not exist at path: {args.location}")
        return RC_ARGUMENT_ERROR

    sortType: SortType
    try:
        sortType = SortType[args.sort_type.upper()]
    except KeyError:
        print(f'Illegal sort type "{args.sort_type}"')
        return RC_ARGUMENT_ERROR

    parser: etree.XMLParser = etree.XMLParser(remove_blank_text=True)
    tree: etree._ElementTree = etree.parse(args.location, parser)
    playlists: list[etree._Element] = xml_get_playlists(tree, args.playlist)
    if args.playlist and not playlists:
        print(f'Could not find playlist "{args.playlist}"')
        return RC_PLAYLIST_NOT_FOUND

    playlist: etree._Element
    for playlist in playlists:
        songs = get_songs(playlist, sortType=sortType)
        songs.sort()
        xml_update(playlist, songs)

    xml_write(args.location, tree, dryRun=args.dry_run)

    return RC_NORMAL

if __name__ == '__main__':
    sys.exit(main())
