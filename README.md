# Rhythmbox Playlist Sort

`playlist-sort.py` is a utility for sorting static Rhythmbox playlists by
sorting the backing XML file that Rhythmbox uses to store its playlists.

Rhythmbox unfortunately offers no mechanism to sort static playlists. All
sorting must be done manually instead. This utility offers a way around that
limitation.

Because sorting is done on the XML Rhythmbox uses, it is suggested Rhythmbox
is closed when this utility is used.

It is also suggested that a backup is made of the playlists XML before
executing the utility, if the sorting order is not as desired or the utility
fails and corrupts the XML file. There is no recovery method for the original
playlist sort order once the XML is overwritten. By default the playlists XML
file can be found at `~/.local/share/rhythmbox/playlists.xml`.

## Usage

`python3 playlist-sort.py --playlist "Hits 1985"`

All static playlists can be sorted at once using the `--all` flag:
`python3 playlist-sort.py --all`

"Standard" sort is the default sorting method, and is a custom sort not found
in Rhythmbox:

1. Artist Name (Case-insensitive)
2. Album Year
3. Album Name (Case-insensitive)
4. Song Disk Number
5. Song Track Number
6. Song Title (Case-insensitive)

The following sorting methods are also available through `--sort-type`:

* Artist
* Album
* (Song) Title

For more options, `python3 playlist-sort.py --help`.

## Dependencies

Dependency version listed is the version used during testing, not necessarily
the minimum-viable version.

* lxml      v4.9.2
* mutagen   v1.46.0

## Disclaimers

### Remote Songs

Rhythmbox supports remote URIs in playlists. This utility parses the files on
disk to discover songs' metadata, it does not use Rhythmbox's database, and
so remote songs are not supported. Such an error will appear as an IO error
when the file is not found.

### Audio File Tags

The following audio file formats can be parsed for tags:

- MPEG/MP3
- MPEG-4/MP4
- Opus

AIFF files are read as MP3 tags. Other types are not supported.

Audio file tags are notoriously non-uniform. The parsers written (wrappers
around Mutagen, which does the actual parsing) match what's found in my
library, but will not match every song in every library. Since many tags may
be missing for songs that need sorting, the utility will report no errors if
tags are parsed improperly, unless Mutagen fails to parse tags out of the file
entirely.

Therefore, it is recommended to quickly check a sorted playlist after first
usage, as there may be egregious errors if your audio file tags differ
significantly from my own.

### Sort Order

An attempt has been made to generally match how Rhythmbox sorts columns.
Currently, the columns supported are:

- Artist
- Album
- Title

However, the exact algorithm(s) Rhythmbox uses was/were not re-implemented,
and there are known differences:

- If a field it blank, Rhythmbox sorts it under the string "Unknown <>".
  This utility will sort it to the bottom of the list instead.
  This is not considered a bug, but an enhancement.

- Support for non-ASCII characters exists, but is known to be incomplete. I am
  sure there are errors there, but it works for the limited set of
  non-ASCII artists/albums/titles my library contains.
  If you have a large J-Pop collection, check your sorted list closely for
  errors.

Like Rhythmbox, if no song title tag can be found, then the filename is used
instead.

There is no support for "sort order" tags that may be found within audio files.

Album-artist tags are only used for sorting if artist tags are not available.

Album year tags must be a year, e.g. "2000", and not a date. If not a year,
any sort that utilizes album year to operate may fail to sort that entry
accurately.

## Why isn't this a Rhythmbox plugin?

It should be! It started out that way. But the Rhythmbox Python API is simply
too poorly documented for me to make it work. I spent a whole day and got
absolutely nowhere. It took longer to get my hands on a playlist QueryModel
in the Rhythmbox API than it did to get a working proof-of-concept using this
XML manipulation method.

At the end of the day this script satisfies my needs and avoids unnecessary
UI modifications. I only ever sort playlists when I'm adding/removing songs,
and so having a separate utility works well.

I would like to see a Rhythmbox plugin to sort static playlists. I would
use such a plugin instead of this script, but this script works well enough.

# License

This repository is licensed under the Unlicense, See LICENSE.txt for more
information.
